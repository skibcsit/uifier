package ru.mephi.csit.uifier

import cats.data.ValidatedNel
import cats.effect.SyncIO
import cats.syntax.validated._
import colibri.Observable
import outwatch.reactive.handler.Handler
import simulacrum._
import scala.language.implicitConversions

// в данном примере ориентируемся на построение интерфейса через OutWatch
import outwatch.VDomModifier
import outwatch.dsl._

/**
 * Инкапсулирует фрагмент интерфейса. В данной примере аналогичен просто типу
 * `VDomModifier`, однако использование `Presenter` позволит в последствии
 * абстрагироваться от `VDomModifier`, заменив его на параметр
 */
trait Presenter {
  def present: VDomModifier
}

/**
 * Класс типов, поддерживающих визуализацию в интерфейсе
 */
trait Presentable[T] {
  def presenter(t: T): Presenter
}

object Presentable {

  def apply[T](presentT: T => VDomModifier): Presentable[T] =
    t => new Presenter { val present: VDomModifier = presentT(t) }

  // здесь идут (возможно, implicit?) объявления стандартных представлений для атомарных типов

  def stringPresentable: Presentable[String] = Presentable(span(_))

}

/**
 * Редактор объектов типа `T`. Т. к. редактор представляет собой фрагмент
 * интерфейса, он является заодно и `Presenter`. Для заданного типа может бить
 * несколько презентеров — один для просмотра данных (`Presenter`), другой для
 * просмотра и редактирования (`Editor`). Кроме того, можно предлагать разные
 * варианты презентеров и редакторов. Автоматический генератор будет позволять
 * генерировать стандартные варианты редакторов и презентеров.
 *
 * Здесь и далее на `F` нужно будет наложить требуемые реализацией ограничения
 * (Sync / Async или др.)
 */
trait Editor[T] extends Presenter {

  /**
   * Поток результатов редактирования, либо сообщений об ошибке. В каких случаях
   * выводить ошибку, зависит от конкретной реализации.
   */
  def result: Observable[ValidatedNel[String, T]]
}

// за счет simulacrum (https://github.com/typelevel/simulacrum)
// автоматизируется генерацию вспомогательного кода для классов типов

/**
 * Класс типов, поддерживающих интерфейс редактирования
 */
@typeclass
trait Editable[T] {
  def editor(t: T): SyncIO[Editor[T]]
}

object Editable {

  // здесь идут объявления стандартных редакторов для атомарных типов

  implicit val stringEditable: Editable[String] =
    (t: String) =>
      for {
        handler <- Handler.create[String](t)
      } yield
        new Editor[String] {
          def result: Observable[ValidatedNel[String, String]] =
            handler.map(_.valid)
          def present: VDomModifier =
            input(`type` := "text", value := t, onChange.value --> handler)
        }

}

/**
 * Класс типов, поддерживающих интерфейс создания, т. е. редактирование может
 * начинаться с отсутствующего объекта. Не путать с редактированием
 * необязательного объекта (`Option[T]`), для которого опционально как начальное
 * значение, так и результат, т. е. оно соответствует просто
 * `Editor[Option[T]]`.
 */
@typeclass
trait Creatable[T] {
  def editor(t: Option[T]): SyncIO[Editor[T]]
  def creator: SyncIO[Editor[T]] = editor(None)
}
