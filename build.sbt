lazy val root = project
  .in(file("."))
  .enablePlugins(ScalaJSBundlerPlugin)
  .settings(
    organization := "ru.mephi.csit",
    name := "uifier",
    scalaVersion := "2.13.7",
    resolvers += "jitpack" at "https://jitpack.io",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "simulacrum" % "1.0.1",
      "com.github.outwatch.outwatch" %%% "outwatch" % "1.0.0-RC4",
    ),
    scalafmtOnCompile := !insideCI.value,
    scalacOptions ++= Seq(
      "-Ymacro-annotations",
      "-feature",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(),
    scalaJSUseMainModuleInitializer := true,
    Compile / mainClass := Some("ru.mephi.csit.uifier.UifierIOApp"),
  )

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}
